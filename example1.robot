*** Settings ***
Library  AutoItLibrary
Library  SikuliLibrary

# Test Setup    Acessar Sistema

*** Variables ***
${Usuario}        admin
${Senha}          123456

*** Test Cases ***
Cenário 1: Login válido
  [tag]   smoke
  Dado que usuário possa acessar a tela de login do sistema
  Quando informar um ${Usuario} válido
  E inserir uma ${Senha}
  Então o sistema deve permitir a autenticação do usuário

Cenário 2: Cadastrar Cliente
  Keyword Test

Cenário 3: Pesquisar Contrato
  Keyword Test

# Cenário 4: Sikuli
#   Teste Sikuli
#   [teardown]    Stop Remote Server

*** Keyword ***
Dado que usuário possa acessar a tela de login do sistema
  Log  teste

Keyword Test
  Log  teste

Teste Sikuli
  Add Image Path      ${CURDIR}/../images/
  Click               image1.png
