*** Settings ***
Library  Process
Library  AutoItLibrary
Library  SikuliLibrary
Library  Screenshot

Suite Setup       Configuracao inicial do ambiente

Test Setup        Acessar Sistema
Test Teardown     Run Keywords    Take Screenshot  ${CURDIR}/../results/
...               AND  Fechar Sistema

Suite Teardown    Stop Remote Server
*** Variables ***
${App}            C:\\SistemaDelphi\\GestaoProjetos.exe
${PATH}           C:\\SistemaDelphi\\
${Servidor}       Servidor1
${Usuario}        Dor_poa
${Senha}          1635123
${Senha2}         1111111
${mensagemErro}   Senha Inválida!
## Tela ##
${telaServidor}   Selecione o banco de dados
${telaSistema}    Sistema de Gestão de Projetos
${telaLogin}      Logon Sistema
## Controls ##
${rdbServidor}            TGroupButton2
${bntSelecionarServidor}  TBitBtn2


*** Test Cases ***
Cenário 1: Login válido
  Dado que usuário possa acessar a tela de login do sistema
  Quando informar um ${Usuario} válido
  E inserir uma ${Senha}
  Então o sistema deve permitir a autenticação do usuário

Cenário 2: Login inválido
  Dado que usuário possa acessar a tela de login do sistema
  Quando informar um ${Usuario} válido
  E inserir uma "11111"
  Então o sistema deve exibir a ${mensagemErro}

*** Keyword ***
Acessar Sistema
  ${Process}            Start Process    ${App}   cwd=${PATH}
  Set Suite Variable    ${Process}
  ## Check 1
  Win Wait Active       ${telaServidor}
  Control Focus         ${telaServidor}  ${Servidor}  ${rdbServidor}
  Control Click         ${telaServidor}  ${EMPTY}     ${bntSelecionarServidor}
  ## Check 2
  Win Wait Active       ${telaSistema}

Fechar Sistema
  Sleep                 5
  Terminate Process     ${Process}  kill=True

Configuracao inicial do ambiente
  Add Image Path                ${CURDIR}/../images/
  Set Capture Matched Image     false

########### Cenário 1 ##############
Dado que usuário possa acessar a tela de login do sistema
  Win Wait Active       ${telaLogin}


Quando informar um ${Usuario} válido
  Send                  ${Usuario}
  Send                  {TAB}

E inserir uma ${Senha}
  Send                  ${Senha}
  Send                  !l

Então o sistema deve permitir a autenticação do usuário
  Wait Until Screen Contain    LoginOK.png    30

  ## Didático ##
  Highlight                    LoginOK.png
  Sleep                        2
  Clear Highlight              LoginOK.png

Então o sistema deve exibir a ${mensagemErro}
  Win Wait Active    ${EMPTY}   WindowText=${mensagemErro}
